#pragma semicolon 1

// Includes
#include <sourcemod>
#include <morecolors>
#include <steamtools>

// Definitions
#define PLUGIN_VERSION "1.0"
#define OCU_URL "http://www.ocu.tf/api/pricecheck"
#define PLUGIN_TESTMODE

// Plugin Info.
public Plugin:myinfo = {
  name = "OCU.TF Server Plugin",
  author = "Luop90",
  version = PLUGIN_VERSION,
  description = "Enables various interaction with ocu.tf",
  url = "http://ocu.tf/plugin"
};

// Global Variables
new Handle:cvarVersion,
    Handle:cvarEnableAdvertisement,
    Handle:cvarAdvertisementTime,
    Handle:cvarTag,
    Handle:sv_tags;

new String:adverts[3] = {
  "Type !ocu to check prices on OCU.TF!",
  "Want fast, secure trading? Head over to OCU.TF today!",
  "With the smartest bots and the fastest trades, OCU.TF is the place to go for all of your trading needs!",
  "Unbeatable prices, fast & secure trading - OCU.TF!"
};

public OnPluginStart() {
  cvarVersion = CreateConVar("ocu_version", PLUGIN_VERSION, "Version of the plugin.");
  HookConVarChange(cvarVersion, OnVersionChanged);

  cvarEnableAdvertisement = CreateConVar("ocu_adverts_enable", "1", "Enable chat advertisements?");
  cvarAdvertisementTime = CreateConVar("ocu_adverts_time", "120", "Time (in seconds) between each advertisement.");

  AddCommandListener(command_pricecheck, "sm_ocu");
  AddCommandListener(command_pricecheck, "sm_ocups");
}

public OnConfigsExecuted() {
  CreateTimer(2.0, checkTag); // Let everything execute...
}

public OnVersionChanged(Handle:cvar, const String:oldVal[], const String:newVal[]) {
	if (strcmp(newVal, PLUGIN_VERSION)) {
		SetConVarString(cvarVersion, PLUGIN_VERSION);
		LogMessage("Plugin version changed back to default");
	}
}

public Action:command_pricecheck(client, const String:cmd[], argc) {
  if (argc == 0) {
    CReplyToCommand(client, "{blue}[{orange}OCU{blue}] {default}Usage: sm_ocu <item>");
    return;
  }

  new String:name[64];
  new String:steamid[64];
  new String:argc[256];

  getCommandArgString(argc, sizeof(argc));
  StripQuotes(argc);

  GetClientAuthId(client, AuthId_Steam64, steamid, sizeof(steamid));

  /**
   * Creates the HTTP Request.
   */
  new HTTPRequestHandle:request = Steam_CreateHTTPRequest(HTTPMethod_GET, OCU_URL);
  Steam_SetHTTPRequestGetOrPostParameter(request, "name", query);
	Steam_SetHTTPRequestGetOrPostParameter(request, "steamid", steamid);
	Steam_SendHTTPRequest(request, OnOCUResponse, GetClientUserId(client));

  return PLUGIN_HANDLED;
}

public OnOCUResponse(HTTPRequestHandle:request, bool:successful, HTTPStatusCode:status, any:uid) {
  new client = GetClientOfUserId(uid);

  // If client disconnected.
  if(client == 0) {
    Steam_ReleaseHTTPRequest(request);
    return;
  }

  // Checks for any errors.
  if(!successful || status != HTTPStatusCode_OK) {
    if(status == HTTPStatusCode_InternalServerError) {
      CPrintToChat(client, "{blue}[{orange}OCU{blue}] {default}An interal server error occured.");
    } else if (status == HTTPStatusCode_OK && !successful) {
      CPrintToChat(client, "{blue}[{orange}OCU{blue}] {default}No data was returned");
    } else {
      CPrintToChat(client, "{blue}[{orange}OCU{blue}] {default}Unable to connect to the server, or no data was sent");
    }

    Steam_ReleaseHTTPRequest(request);
    return;
  }

  // Get the size of the response.
  new bSize = Steam_GetHTTPResponseBodySize(request);
	decl String:data[bSize];

	Steam_GetHTTPResponseBodyData(request, data, bSize);
	Steam_ReleaseHTTPRequest(request);

  #ifdef PLUGIN_TESTMODE
    PrintToChatAll("{blue}[{orange}OCU{blue}] {default}Query success: (Client: %N) %s", client, data);
  #endif

  // Parse out our "JSON" from the response.
}
