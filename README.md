# OCU Server Plugin
This is the official server plugin for [OCU.tf](http://ocu.tf)

## What does this do?
Primarily, this periodically displays ads in chat about OCU. In addition, this allows for users to check our current item prices directly from the server.

## Usage
1. Download the source file, and compile it yourself
  -OR-
  Download the compiled file
2. Place in tf/addons/sourcemod/plugins
3. Restart the server

## License
Released under the terms of the [MIT License](https://opensource.org/licenses/MIT)
